from hdfs import InsecureClient
from pathlib import Path
import time

def write_file(client: InsecureClient, local_path: str, hdfs_path: str = None) -> None:
    cwd = Path(local_path)

    for file in cwd.rglob('*.*'):
        client.upload(hdfs_path + file.name,
                      str(file),
                      overwrite=True)
        print(f'uploading {file.name}')

root_path = "/"
url = 'http://ec2-3-138-69-198.us-east-2.compute.amazonaws.com:50070/'

client = InsecureClient(url=url, user='ubuntu', root=root_path)

print(client.list(f'{root_path}'))
# Create a directory
# client.makedirs(f'{root_path}test_folder')
# print(client.list(f'{root_path}'))
# client.upload(f'{root_path}', '/home/asemenov/Course_job/files_example/man.jpg')
print(client.list(f'{root_path}test_folder'))
print(client.read('wood.png'))
# with client.read('wood.png') as reader:
#     features = reader.read()
client.upload('/test_upload.jpg',
              'files_example/man.jpg',
              overwrite=True)















#    :: File path
# Second parameter: file content
# Third parameters: Whether to override existing files, if not overwritten, and the file already throws an exception
# c.write('/user/root/pyhdfs/1.log', time.asctime(time.localtime(time.time())) + '\n', True)



# print(client.list(f'{root_path}'))
# # client.makedirs(f'/{user}/{root_path}/test_folder')
# # client.write('/user/root/pyhdfs/',time.asctime(time.localtime(time.time())) + '\n',True)
# client.upload('/asemenov/test_folder/', './files_example/man.jpg', False)
# # # print(client.list('.'))
# hdfs_files = client.list('/asemenov/test_folder', True)
# for f in hdfs_files:
#     print(f)
# print(client.set_acl('/hadoop_test.txt'))
# # print(client.acl_status('/'))
# # print(client.parts('/'))
# # print(client.content('/'))
# # Loading a file in memory.
# with client.read('/user/root/hadoop_test.txt', encoding='utf-8') as reader:
#     features = reader.read()
# client.upload('/', './files_example/man.jpg')
# hdfs_path='/'
# with client.read('/user/ml_andrey/hadoop_test.txt') as reader:
#     file = reader.read()

# cwd = Path('./files_example')
# for file in cwd.rglob('*.*'):
#     print(file.name)
#     client.upload(hdfs_path + file.name,
#                   str(file),
#                   overwrite=True)
# web_hdfs_interface = InsecureClient('http://localhost:9870')
# cwd = Path('./files_example')
# destination_path = '/user/asemenov/data'
# print(web_hdfs_interface.list('/'))
#
#
# web_hdfs_interface.download('hadoop_text.txt',
#                             './files_example')

# try:
#     web_hdfs = InsecureClient('http://localhost:9870', user='ml_andrey')
#     print('Выполнилось подключение к кластеру Hadoop')
#     files_list = web_hdfs.list('/')
#     if len(files_list) > 0:
#         print(f"Список файлов в hdfs:\n{files_list}")
#     else:
#         print("Корневая директория не имеет данных")
#
#     try:
#         write_file(
#             client=web_hdfs,
#             local_path='./files_example',
#             hdfs_path='/'
#         )
#     except Exception as e:
#         print(f'Не удалось записать файлы')
#
# except Exception as e:
#     print('Подключение не удалось выполнить')
